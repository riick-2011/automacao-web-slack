require 'capybara'
require 'capybara/cucumber'
require 'selenium-webdriver'
require 'rspec'
require 'faker'
require 'report_builder'
require 'site_prism'
require 'json'
require 'pry'
require 'capybara/rspec/matchers'
require_relative 'page_helper.rb'
require 'httparty'

World Page
World Capybara::DSL
World Capybara::RSpecMatchers
BROWSER = ENV['BROWSER']
AMB = ENV['AMB']

BASE_URL = YAML.load_file(File.dirname(__FILE__) + "/ambientes/ambientes.yml")[AMB]
ELL = YAML.load_file(File.dirname(__FILE__) + "/loads/elementos.yml")
LOADS = YAML.load_file(File.dirname(__FILE__) + "/loads/loads.yml")


if BROWSER == "firefox"
  @driver = :selenium  
elsif BROWSER == "chrome"
  Capybara.register_driver :selenium_chrome do |app|
    Capybara::Selenium::Driver.load_selenium
    browser_options = ::Selenium::WebDriver::Chrome::Options.new.tap do |opts|
      opts.args << '--start-maximized'
    end
    Capybara::Selenium::Driver.new(
      app,
      browser: :chrome, 
      options: browser_options
    )
  end
  @driver = :selenium_chrome
elsif BROWSER == "chrome_two"
  Capybara.register_driver :selenium_chrome_headless do |app|
    Capybara::Selenium::Driver.load_selenium
    browser_options = ::Selenium::WebDriver::Chrome::Options.new.tap do |opts|
      opts.args << '--headless'
      opts.args << '--disable-gpu' if Gem.win_platform?
      opts.args << '--no-sandbox'
      opts.args << '--incognito'
      opts.args << '--window-size=1200x680'
      opts.args << '--disable-site-isolation-trials'
    end
    Capybara::Selenium::Driver.new(
      app,
      browser: :chrome,
      options: browser_options
    )
  end
  @driver = :selenium_chrome_headless
else ""
  raise "\n\nPor favor informe um BROWSER \n\n"

end

ReportBuilder.configure do |config|
  config.json_path = "results/report.json" #pasta onde salva o json
  config.report_path = "results/report" #pasta onde salva o html
  config.report_types = [:html] #tipo de report a exportar
  config.report_title = "Report de testes" #nome do report
  config.color = "blue" #cor do report
  config.include_images = true #se coloca imagens ou não
  #config.additional_info = { browser: BROWSER, ambiente: AMB, user: "Agnaldo Vilariano"} 
end

#essa variável ambiente recebe uma configuração do ENV. voce pode apagar os IFs ali e deixar o nome chumbado
at_exit do
  ReportBuilder.build_report
end

Capybara.configure do |config|
    config.default_driver = @driver
    config.app_host = BASE_URL["base_url"]

end
  