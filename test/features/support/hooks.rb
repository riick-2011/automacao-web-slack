After do |scenario|
    scenario_name = scenario.name.gsub(/[^\w\-]/, ' ')
    if scenario.failed?
        take_a_picture(scenario_name.downcase!, 'Falhou')
    else
        take_a_picture(scenario_name.downcase!, 'Sucesso')
    end
end

def take_a_picture(file_name, resultado)
    data = Time.now.strftime('%F').to_s
    h_m_s = Time.now.strftime('%H%M%S%p').to_s
    temp_shot = page.save_screenshot("results/evidencia/#{data}/temp_#{h_m_s}.png")

    attach(temp_shot, 'image/png')
end

def screenshot()
    data = Time.now.strftime('%F').to_s
    h_m_s = Time.now.strftime('%H%M%S%p').to_s
    temp_shot = page.save_screenshot("results/evidencia/#{data}/step/temp_#{h_m_s}.png")

    attach(temp_shot, 'image/png')
end


at_exit do 
    total_cenario = 0
    sucess_cenario = 0 
    fail_cenario = 0
    total_step = 0 
    sucess_step = 0 
    fail_step = 0 
    skiped_step = 0
    undefined_step = 0
    report_path = "#{Dir.pwd}/results"
    report = JSON.parse(File.read("#{report_path}/report.json")) 
    report.each do |feature|
        feature["elements"].each do |cenario| 
            total_cenario += 1
            if cenario["steps"].last["result"]["status"] == "passed"
                sucess_cenario += 1
            elsif cenario["steps"].last["result"]["status"] == "failed"
                fail_cenario += 1
            end
            cenario["steps"].each do |step|
                total_step += 1
                if step["result"]["status"] == "passed"
                    sucess_step += 1
                elsif step["result"]["status"] == "failed"
                    fail_step += 1
                elsif step["result"]["status"] == "undefined"
                    undefined_step += 1
                elsif step["result"]["status"] == "pending"
                    skiped_step += 1
                end
            end
        end
    end
    color = if fail_cenario >= 1
        "#FF0000"
    else
        "#1FFF00"
    end
    nao_concluido = skiped_step + undefined_step
    data = Time.now.strftime('%F').to_s
    $payload = {
        "attachments": [
            {
                "color": "#{color}", 
                "title": "Ambiente de Execução #{AMB}"
            },
            {
                "color": "#{color}", 
                "title": "Link - Pipelines",
                "title_link": "https://gitlab.com/riick-2011/automacao-web-slack/-/pipelines"
            },
            {
                "color": "#{color}", 
                "title": "Branch",
                "title_link": "https://gitlab.com/riick-2011/automacao-web-slack"
            },
            {
                "color": "#{color}",
                "title": "Resultado dos testes:",
                "text": "Data dos testes: #{data}",
                "fields": [
                    {
                        "title": "Cenário",
                        "value": "Total\nSucesso\nFalhas\n",
                        "short": true
                    },
                    {
                        "title": "Quantidade",
                        "value": "#{total_cenario}\n#{sucess_cenario}\n#{fail_cenario}\n",
                        "short": true
                    },
                    {
                        "title": "Steps",
                        "value": "Total\nSucesso\nFalhas\nNão Concluído\n",
                        "short": true
                    },
                    {
                        "title": "Step",
                        "value": "#{total_step}\n#{sucess_step}\n#{fail_step}\n#{nao_concluido}\n",
                        "short": true
                    }
                ]
            }
        ]
    }
    @return = HTTParty.post(BASE_URL["api_slack"], body: $payload.to_json)  
end