class AcessoSucesso < SitePrism::Page
    set_url ' '
    element :cmp_login, '#login-form>section>div:nth-child(2)>div.col-md-6>input'
    element :cmp_senha, '#login-form>section>div:nth-child(3)>div.col-md-6>div>input'
    
    def login_acesso(login, passw)
        find(ELL['sing_in']).click
        cmp_login.set login
        cmp_senha.set passw
        find(ELL['btn_subm']).click
    end

    
end

