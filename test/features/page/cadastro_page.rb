class CadastroCompletpo < SitePrism::Page
    
    set_url ''
    element :lnk_create, '#content>div>a'
    element :name, '#customer-form>section>div:nth-child(2)>div.col-md-6>input'
    element :last, '#customer-form>section>div:nth-child(3)>div.col-md-6>input'
    element :e_mail, '#customer-form>section>div:nth-child(4)>div.col-md-6>input'
    element :niver, '#customer-form>section>div:nth-child(6)>div.col-md-6>input'
    element :passw, '#customer-form>section>div:nth-child(5)>div.col-md-6>div>input'
    element :btn_save, '#customer-form>footer>button'
    element :out, '#_desktop_user_info>div>a.logout.hidden-sm-down'
    def cadastro_novo
        find(ELL['btn_singin']).click
        lnk_create.click
        page.execute_script("$('#customer-form>section>div:nth-child(1)>div.col-md-6.form-control-valign>label:nth-child(1)').click();")
        name.set Faker::Name.first_name
        last.set Faker::Name.last_name
        @login = Faker::Internet.email
        e_mail.set @login
        LOADS['login']<< @login
        niver.set Faker::Date.birthday(min_age: 24, max_age: 55)
        @senha = '123456'
        passw.set @senha
        LOADS['senha'] << @senha
        page.execute_script("$('#customer-form > section > div:nth-child(9) > div.col-md-6 > span > label > input[type=checkbox]').click();")
        btn_save.click
    end

    def log_out 
        out.click
    end
    
end